import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule }    from '@angular/http';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from '../components/app.component';
import { LoginComponent } from '../components/login.component';
import { TasksComponent } from '../components/tasks.component';

import { routing } from '../routing/app.routing';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        TasksComponent
    ],
    bootstrap: [ AppComponent ]
})

export class AppModule {
}