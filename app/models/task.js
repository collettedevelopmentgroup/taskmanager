"use strict";
var Task = (function () {
    function Task(values) {
        if (values === void 0) { values = {}; }
        this.id = values['id'] || null;
        this.description = values['description'] || '';
        this.completed_at = values['completed_at'] ? true : false;
    }
    Task.prototype.toggleCompletedAt = function () {
        this.completed_at = !this.completed_at;
    };
    return Task;
}());
exports.Task = Task;
//# sourceMappingURL=task.js.map