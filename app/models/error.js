"use strict";
var Error = (function () {
    function Error(error) {
        this.id = this.guid();
        this.message = error;
    }
    Error.prototype.guid = function () {
        return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
            this.s4() + '-' + this.s4() + this.s4() + this.s4();
    };
    Error.prototype.s4 = function () {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    };
    return Error;
}());
exports.Error = Error;
//# sourceMappingURL=error.js.map