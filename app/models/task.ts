export class Task {
    id: number;
    description: string;
    completed_at: boolean;

    constructor(values: Object = {}) {
        this.id = values['id'] || null;
        this.description = values['description'] || '';
        this.completed_at = values['completed_at'] ? true : false;
    }

    toggleCompletedAt() {
        this.completed_at = !this.completed_at;
    }
}