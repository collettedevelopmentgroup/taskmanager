import { Task } from './task';

export class User {
    id: number;
    email: string;
    name: string;
    tasks: Task[];

    constructor(values: Object = {}) {
        this.id = values['id'] || null;
        this.email = values['email'] || '';
        this.name = values['name'] || '';
        this.tasks = [];

        if (values['tasks'] && values['tasks'].length > 0) {
            for (var i=0; i<values['tasks'].length; i++) {
                this.tasks.push(new Task(values['tasks'][i]))
            }
        }
    }
}