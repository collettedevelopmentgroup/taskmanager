"use strict";
var task_1 = require('./task');
var User = (function () {
    function User(values) {
        if (values === void 0) { values = {}; }
        this.id = values['id'] || null;
        this.email = values['email'] || '';
        this.name = values['name'] || '';
        this.tasks = [];
        if (values['tasks'] && values['tasks'].length > 0) {
            for (var i = 0; i < values['tasks'].length; i++) {
                this.tasks.push(new task_1.Task(values['tasks'][i]));
            }
        }
    }
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.js.map