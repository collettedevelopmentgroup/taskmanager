import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


import { User } from '../models/user';

@Injectable()

export class UserService {
    constructor(private http: Http) { }

    private taskManagerApi = 'http://taskmanager-api.ty';

    getUser(id: number) {
        return this.http.get(this.taskManagerApi + '/api/users/' + id)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    createOrFindUser(email: string) {
        let payload = JSON.stringify({ "email" : email });
        let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.taskManagerApi + '/api/users', payload, options)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));;
    }

    updateUser(user: User) {
        let payload = JSON.stringify({ "email" : user.email, "name": user.name });
        let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.put(this.taskManagerApi + '/api/users/' + user.id, payload, options)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));;
    }
}