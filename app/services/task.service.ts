import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


import { Task } from '../models/task';
import { User } from '../models/user';

@Injectable()

export class TaskService {
    constructor(private http: Http) { }

    private taskManagerApi = 'http://taskmanager-api.ty';

    getTask(id: number) {
        return this.http.get(this.taskManagerApi + '/api/tasks/' + id)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    createTask(task: Task, user: User) {
        let payload = JSON.stringify({ "description" : task.description, "user_id": user.id });
        let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.taskManagerApi + '/api/tasks', payload, options)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    updateTask(task: Task) {
        let payload = JSON.stringify({ "description" : task.description, "completed_at": task.completed_at });
        let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.put(this.taskManagerApi + '/api/tasks/' + task.id, payload, options)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }

    deleteTask(task: Task) {
        let headers = new Headers({ 'Content-Type': 'application/json', 'Accept': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.delete(this.taskManagerApi + '/api/tasks/' + task.id, options)
            .map((res: Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
}