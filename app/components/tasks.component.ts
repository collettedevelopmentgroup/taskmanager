import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { User } from '../models/user';
import { Error } from '../models/error';
import { Task } from '../models/task';

import { UserService } from '../services/user.service';
import { TaskService } from '../services/task.service';

@Component({
    template: `
<div *ngFor="let error of errors " class="alert alert-danger">
    <button type="button" class="close" (click)="alertClickHandler(error)">×</button>
    <strong>Oh snap!</strong> {{ error.message }}
</div>
<div *ngIf="!currentUser.name || showUpdateUserName" class="alert alert-warning">
    <h4>Hey, wait a minute...</h4>
    <p style="margin-bottom: 15px;">I don't know what your name is! What's your name?</p>
    <div class="form-group">
        <input [(ngModel)]="currentUser.name" type="text" class="form-control" placeholder="Your name goes here" />
    </div>
    <p style="min-height: 35px"><button (click)="updateUserWithName()" class="btn btn-warning pull-right" type="button">This is my name!</button></p>
</div>
<h4><span>{{ currentUser.name || 'Person' }}</span>'s Tasks</h4>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="input-group">
            <input type="text" [(ngModel)]="newTask.description" class="form-control" placeholder="Add a new Task here!">
            <span class="input-group-btn">
                <button (click)="addNewTask()" class="btn btn-primary" type="button"><span class="glyphicon glyphicon-plus" style="min-height: 20px;font-size: 16px;font-weight: 900;"></span></button>
            </span>
        </div><!-- /input-group -->
    </div>
    <div class="panel-body">
        <div style="box-sizing: border-box;min-height: 20px;padding: 19px;background-color: rgb(222, 222, 222);border: 1px solid rgb(204, 204, 204);border-radius: 4px;box-shadow: 0px 1px 1px rgba(0, 0, 0, .10) inset;">
            <ul class="list-group" style="margin: 0;border-radius: 4px; -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);box-shadow: 0 1px 2px rgba(0,0,0,.2);-webkit-border-radius: 4px;-moz-border-radius: 4px;">
                <li *ngFor="let task of currentUser.tasks" class="list-group-item" [style.background-color]="task.completed_at ? '#efefef': '#fff'">
                    <button *ngIf="task.completed_at" type="button" (click)="deleteTaskEvt(task)" class="close">×</button>
                    <div style="margin-left: 15px; width: 50%;">
                        <div class="checkbox">
                            <label [style.text-decoration]="task.completed_at ? 'line-through': 'none'">
                                <input (click)="updateTaskEvt(task)" [(ngModel)]="task.completed_at" type="checkbox" style="margin-left: -26px;margin-top: 2px;">{{ task.description }}
                            </label>
                        </div>
                    </div>
                </li>
                <li *ngIf="currentUser.tasks.length === 0" class="list-group-item">You have no tasks - add one!</li>
            </ul>
        </div>
    </div>
</div>
    `,
providers: [ UserService, TaskService ]
})

export class TasksComponent implements OnInit {
    currentUser: User;
    newTask: Task;
    errors: Error[];
    showUpdateUserName: boolean = false;

    constructor(private userService: UserService, private route: ActivatedRoute, private taskService: TaskService) { }

    ngOnInit() {
        this.currentUser = new User();
        this.newTask = new Task();

        this.route.params.forEach((params: Params) => {
            let id = +params['id'];
            this.userService.getUser(id).subscribe(
                (resp) => {
                    console.log(resp);

                    if (resp.status == "SUCCESS") {
                        this.currentUser = new User(resp.response);

                        if (!this.currentUser.name)
                            this.showUpdateUserName = true;
                    } else {
                        this.errors.push(new Error(resp.response.message));
                    }
                },
                err => console.log(err)
            );
        });
    }

    updateUserWithName() {
        this.userService.updateUser(this.currentUser).subscribe(
            (resp) => {
                console.log(resp);

                if (resp.status == "SUCCESS") {
                    this.showUpdateUserName = false;
                } else {
                    this.errors.push(new Error(resp.response.message));
                }
            },
                err => console.log(err)
        );
    }

    addNewTask() {
        this.taskService.createTask(this.newTask,this.currentUser).subscribe(
            (resp) => {
                console.log(resp);

                if (resp.status == "SUCCESS") {
                    this.currentUser.tasks.push(new Task(resp.response));
                    this.newTask = new Task();
                } else {
                    this.errors.push(new Error(resp.response.message));
                }
            },
                err => console.log(err)
        );
    }

    updateTaskEvt(task: Task) {
        task.toggleCompletedAt()

        this.taskService.updateTask(task).subscribe(
            (resp) => {
                console.log(resp);

                if (resp.status != "SUCCESS") {
                    this.errors.push(new Error(resp.response.message));
                }
            },
                err => console.log(err)
        );
    }

    deleteTaskEvt(task: Task) {
        this.taskService.deleteTask(task).subscribe(
            (resp) => {
                console.log(resp);

                if (resp.status == "SUCCESS") {
                    for (var i=this.currentUser.tasks.length -1; i >= 0; i--) {
                        if (this.currentUser.tasks[i].id == task.id)
                            this.currentUser.tasks.splice(i,1);
                    }
                } else {
                    this.errors.push(new Error(resp.response.message));
                }
            },
                err => console.log(err)
        );
    }
}