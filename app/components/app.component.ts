import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
<h2>{{title}}</h2>
<router-outlet></router-outlet>
`
})

export class AppComponent {
    title = "Task Manager";
}