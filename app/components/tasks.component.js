"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_1 = require('../models/user');
var error_1 = require('../models/error');
var task_1 = require('../models/task');
var user_service_1 = require('../services/user.service');
var task_service_1 = require('../services/task.service');
var TasksComponent = (function () {
    function TasksComponent(userService, route, taskService) {
        this.userService = userService;
        this.route = route;
        this.taskService = taskService;
        this.showUpdateUserName = false;
    }
    TasksComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.currentUser = new user_1.User();
        this.newTask = new task_1.Task();
        this.route.params.forEach(function (params) {
            var id = +params['id'];
            _this.userService.getUser(id).subscribe(function (resp) {
                console.log(resp);
                if (resp.status == "SUCCESS") {
                    _this.currentUser = new user_1.User(resp.response);
                    if (!_this.currentUser.name)
                        _this.showUpdateUserName = true;
                }
                else {
                    _this.errors.push(new error_1.Error(resp.response.message));
                }
            }, function (err) { return console.log(err); });
        });
    };
    TasksComponent.prototype.updateUserWithName = function () {
        var _this = this;
        this.userService.updateUser(this.currentUser).subscribe(function (resp) {
            console.log(resp);
            if (resp.status == "SUCCESS") {
                _this.showUpdateUserName = false;
            }
            else {
                _this.errors.push(new error_1.Error(resp.response.message));
            }
        }, function (err) { return console.log(err); });
    };
    TasksComponent.prototype.addNewTask = function () {
        var _this = this;
        this.taskService.createTask(this.newTask, this.currentUser).subscribe(function (resp) {
            console.log(resp);
            if (resp.status == "SUCCESS") {
                _this.currentUser.tasks.push(new task_1.Task(resp.response));
                _this.newTask = new task_1.Task();
            }
            else {
                _this.errors.push(new error_1.Error(resp.response.message));
            }
        }, function (err) { return console.log(err); });
    };
    TasksComponent.prototype.updateTaskEvt = function (task) {
        var _this = this;
        task.toggleCompletedAt();
        this.taskService.updateTask(task).subscribe(function (resp) {
            console.log(resp);
            if (resp.status != "SUCCESS") {
                _this.errors.push(new error_1.Error(resp.response.message));
            }
        }, function (err) { return console.log(err); });
    };
    TasksComponent.prototype.deleteTaskEvt = function (task) {
        var _this = this;
        this.taskService.deleteTask(task).subscribe(function (resp) {
            console.log(resp);
            if (resp.status == "SUCCESS") {
                for (var i = _this.currentUser.tasks.length - 1; i >= 0; i--) {
                    if (_this.currentUser.tasks[i].id == task.id)
                        _this.currentUser.tasks.splice(i, 1);
                }
            }
            else {
                _this.errors.push(new error_1.Error(resp.response.message));
            }
        }, function (err) { return console.log(err); });
    };
    TasksComponent = __decorate([
        core_1.Component({
            template: "\n<div *ngFor=\"let error of errors \" class=\"alert alert-danger\">\n    <button type=\"button\" class=\"close\" (click)=\"alertClickHandler(error)\">\u00D7</button>\n    <strong>Oh snap!</strong> {{ error.message }}\n</div>\n<div *ngIf=\"!currentUser.name || showUpdateUserName\" class=\"alert alert-warning\">\n    <h4>Hey, wait a minute...</h4>\n    <p style=\"margin-bottom: 15px;\">I don't know what your name is! What's your name?</p>\n    <div class=\"form-group\">\n        <input [(ngModel)]=\"currentUser.name\" type=\"text\" class=\"form-control\" placeholder=\"Your name goes here\" />\n    </div>\n    <p style=\"min-height: 35px\"><button (click)=\"updateUserWithName()\" class=\"btn btn-warning pull-right\" type=\"button\">This is my name!</button></p>\n</div>\n<h4><span>{{ currentUser.name || 'Person' }}</span>'s Tasks</h4>\n<div class=\"panel panel-default\">\n    <div class=\"panel-heading\">\n        <div class=\"input-group\">\n            <input type=\"text\" [(ngModel)]=\"newTask.description\" class=\"form-control\" placeholder=\"Add a new Task here!\">\n            <span class=\"input-group-btn\">\n                <button (click)=\"addNewTask()\" class=\"btn btn-primary\" type=\"button\"><span class=\"glyphicon glyphicon-plus\" style=\"min-height: 20px;font-size: 16px;font-weight: 900;\"></span></button>\n            </span>\n        </div><!-- /input-group -->\n    </div>\n    <div class=\"panel-body\">\n        <div style=\"box-sizing: border-box;min-height: 20px;padding: 19px;background-color: rgb(222, 222, 222);border: 1px solid rgb(204, 204, 204);border-radius: 4px;box-shadow: 0px 1px 1px rgba(0, 0, 0, .10) inset;\">\n            <ul class=\"list-group\" style=\"margin: 0;border-radius: 4px; -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);box-shadow: 0 1px 2px rgba(0,0,0,.2);-webkit-border-radius: 4px;-moz-border-radius: 4px;\">\n                <li *ngFor=\"let task of currentUser.tasks\" class=\"list-group-item\" [style.background-color]=\"task.completed_at ? '#efefef': '#fff'\">\n                    <button *ngIf=\"task.completed_at\" type=\"button\" (click)=\"deleteTaskEvt(task)\" class=\"close\">\u00D7</button>\n                    <div style=\"margin-left: 15px; width: 50%;\">\n                        <div class=\"checkbox\">\n                            <label [style.text-decoration]=\"task.completed_at ? 'line-through': 'none'\">\n                                <input (click)=\"updateTaskEvt(task)\" [(ngModel)]=\"task.completed_at\" type=\"checkbox\" style=\"margin-left: -26px;margin-top: 2px;\">{{ task.description }}\n                            </label>\n                        </div>\n                    </div>\n                </li>\n                <li *ngIf=\"currentUser.tasks.length === 0\" class=\"list-group-item\">You have no tasks - add one!</li>\n            </ul>\n        </div>\n    </div>\n</div>\n    ",
            providers: [user_service_1.UserService, task_service_1.TaskService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.ActivatedRoute, task_service_1.TaskService])
    ], TasksComponent);
    return TasksComponent;
}());
exports.TasksComponent = TasksComponent;
//# sourceMappingURL=tasks.component.js.map