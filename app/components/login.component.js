"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var user_1 = require('../models/user');
var error_1 = require('../models/error');
var user_service_1 = require('../services/user.service');
var LoginComponent = (function () {
    function LoginComponent(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.currentUser = new user_1.User();
        this.errors = [];
    };
    LoginComponent.prototype.alertClickHandler = function (target) {
        for (var i = this.errors.length - 1; i >= 0; i--) {
            if (this.errors[i].id == target.id)
                this.errors.splice(i, 1);
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.userService.createOrFindUser(this.currentUser.email)
            .subscribe(function (resp) {
            console.log(resp);
            if (resp.status == "SUCCESS") {
                _this.router.navigate(['/users', resp.response.id]);
            }
            else {
                _this.errors.push(new error_1.Error(resp.response.message));
            }
        }, function (err) { return console.log(err); });
    };
    LoginComponent = __decorate([
        core_1.Component({
            template: "\n<div *ngFor=\"let error of errors \" class=\"alert alert-danger\">\n    <button type=\"button\" class=\"close\" (click)=\"alertClickHandler(error)\">\u00D7</button>\n    <strong>Oh snap!</strong> {{ error.message }}\n</div>\n<div *ngIf=\"currentUser\">\n<h4>Please enter your email</h4>\n    <div class=\"form-group\">\n        <input [(ngModel)]=\"currentUser.email\" type=\"text\" class=\"form-control\" />\n    </div>\n    <button [disabled]=\"!currentUser.email\" (click)=\"login()\" class=\"btn btn-primary btn-block\" type=\"button\">Get my Tasks!</button>\n</div>\n",
            providers: [user_service_1.UserService]
        }), 
        __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map