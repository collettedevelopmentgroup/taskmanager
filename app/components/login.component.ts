import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { User } from '../models/user';
import { Error } from '../models/error';

import { UserService } from '../services/user.service';

@Component({
    template: `
<div *ngFor="let error of errors " class="alert alert-danger">
    <button type="button" class="close" (click)="alertClickHandler(error)">×</button>
    <strong>Oh snap!</strong> {{ error.message }}
</div>
<div *ngIf="currentUser">
<h4>Please enter your email</h4>
    <div class="form-group">
        <input [(ngModel)]="currentUser.email" type="text" class="form-control" />
    </div>
    <button [disabled]="!currentUser.email" (click)="login()" class="btn btn-primary btn-block" type="button">Get my Tasks!</button>
</div>
`,
providers: [ UserService ]
})

export class LoginComponent implements OnInit {
    currentUser: User;
    errors: Error[];

    constructor(private userService: UserService, private router: Router) { }

    ngOnInit() {
        this.currentUser = new User();
        this.errors = [];
    }

    alertClickHandler(target: Error) {
        for (var i=this.errors.length - 1; i >= 0; i--) {
            if (this.errors[i].id == target.id) this.errors.splice(i,1);
        }
    }

    login() {
        this.userService.createOrFindUser(this.currentUser.email)
            .subscribe(
            (resp) => {
                    console.log(resp);

                    if (resp.status == "SUCCESS") {
                        this.router.navigate(['/users', resp.response.id]);
                    } else {
                        this.errors.push(new Error(resp.response.message));
                    }
                },
                err => console.log(err)
            );
    }
}