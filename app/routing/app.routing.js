"use strict";
var router_1 = require('@angular/router');
var login_component_1 = require('../components/login.component');
var tasks_component_1 = require('../components/tasks.component');
var appRoutes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent
    },
    {
        path: 'users/:id',
        component: tasks_component_1.TasksComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map