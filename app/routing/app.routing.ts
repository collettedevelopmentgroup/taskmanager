import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from '../components/login.component';
import { TasksComponent } from '../components/tasks.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'users/:id',
        component: TasksComponent
    }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);